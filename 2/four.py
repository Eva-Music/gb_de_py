sentence = input("Write sentence: ").split(" ")

for i, word in enumerate(sentence):
    if len(word) > 10:
        print(i + 1, word[:10])
    else:
        print(i + 1, word)