print("Write 'q' if you want to end the sequence")

elems = list()
elem = input("Write something: ")

while elem != "q":
    elems.append(elem)
    elem = input("Write something: ")

print()
print(elems)

size = (len(elems) - 1, len(elems))[len(elems) / 2 % 2 == 0]
for i in range(0, size, 2):
    next = elems[i + 1]
    current = elems[i]
    elems[i + 1] = current
    elems[i] = next

print(elems)