class Cell:
    def __init__(self, cells_count):
        self.cells_count = cells_count

    def __add__(self, other):
        return Cell(self.cells_count + other.cells_count)

    def __sub__(self, other):
        if self.cells_count - other.cells_count > 0:
            return Cell(self.cells_count - other.cells_count)
        else:
            print("Невозможно выполнить вычитание, кол-во клеток станет меньше 0")

    def __mul__(self, other):
        return Cell(self.cells_count * other.cells_count)

    def __truediv__(self, other):
        return Cell(round(self.cells_count / other.cells_count))

    def make_order(self, count_in_row):
        final_string = ''
        times = int(self.cells_count/count_in_row)
        for i in range(times):
            final_string += "*" * count_in_row + '\n'
        final_string += "*" * (self.cells_count - times * count_in_row)
        return final_string


c = Cell(5)
k = Cell(6)
c = c.__add__(k)
print(c.make_order(3))

