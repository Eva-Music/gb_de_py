import random


class Matrix:
    def __init__(self, x, y, M=0):
        self.x = x
        self.y = y
        self.M = (M, self.__form())[M == 0]

    def __form(self):
        a = []
        for i in range(self.y):
            b = []
            for j in range(self.x):
                b.append(random.randint(0, 50))
            a.append(b)
        return a

    def __str__(self):
        matrix = ''
        for row in self.M:
            for elem in row:
                matrix += str(elem) + ' '
            matrix += '\n'
        return matrix

    def __add__(self, other):
        result = [[0] * self.x for i in range(self.y)]
        for i in range(len(self.M)):
            for j in range(len(self.M[0])):
                result[i][j] = self.M[i][j] + other.M[i][j]

        return Matrix(self.x, self.y, result)


m = Matrix(2, 3)
print(m)
l = Matrix(2, 3)
print(l)

print(m + l)
