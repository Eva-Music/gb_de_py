from abc import ABC, abstractmethod


class Clothes(ABC):
    def __init__(self, name):
        self.name = name

    @abstractmethod
    def count(self):
        pass


class Dress(Clothes):
    def __init__(self, name, size):
        super(Dress, self).__init__(name)
        self.size = size

    @property
    def count(self):
        return round(self.size / 6.5 + 0.5, 2)


class Suit(Clothes):
    def __init__(self, name, height):
        super(Suit, self).__init__(name)
        self.height = height

    @property
    def count(self):
        return round(2 * self.height + 0.3, 2)


dress = Dress("Gucci", 5)
suit = Suit("Dolche", 8)
print(dress.count)
print(suit.count)
