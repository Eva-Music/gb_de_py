from functools import reduce


def fact(n):
    return [el for el in range(1, n + 1)]


def multiply(a, b):
    return a * b


g = fact(4)
print(reduce(multiply, g))
print(g)
