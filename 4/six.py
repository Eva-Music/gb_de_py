from sys import argv
from itertools import cycle, count

number = argv


def generator():
    for el in range(20):
        yield el


def count_num(n):
    for el in count(n):
        if el > 20:
            break
        print(el)


def cycle_list(elems):
    i = 0
    for el in cycle(elems):
        if i > 11:
            break
        print(el)
        i += 1


print(count_num(number))
print("__________________________________")
print(cycle_list(generator()))

