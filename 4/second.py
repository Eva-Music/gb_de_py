import random

random_list = [random.randint(0, 50) for iter in range(20)]
print(random_list)


def compare(e):
    i = random_list.index(e)
    if i > 0:
        return e > random_list[i-1]


new_random_list = [el for el in random_list if compare(el)]
print(new_random_list)
