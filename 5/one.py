file_name = "text.txt"

with open(file_name, "w") as file_write:
    text = input("Insert text: ")
    while text != "":
        file_write.write(text + "\n")
        text = input("Insert text: ")

file_read = open(file_name)
print(file_read.read())