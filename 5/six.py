import re
from operator import is_not
from functools import partial, reduce

lessons = {}
with open("text.txt") as file_read:
    lines = file_read.readlines()
    for line in lines:
        info = line.split(":")
        hours = re.split(r'[^\d+]', info[1])
        hours = list(filter(partial(is_not, ''), hours))
        hours_count = reduce(lambda x, y: int(x) + int(y), hours)
        lessons.update({info[0]: hours_count})

print(lessons)