from functools import reduce

file_name = "text.txt"
file_read = open(file_name)
text = file_read.readlines()
emp_dic = {}
for i in text:
    arr = i.split(" ")
    emp_dic.update({arr[0]: arr[1].replace("\n", "")})

for emp, salary in emp_dic.items():
    if int(salary) < 20000:
        print(emp, '- зп меньше 20000')

print(reduce(lambda x, y: (int(x) + int(y)), emp_dic.values())
      / len(emp_dic), '- средняя зп сотрудников')