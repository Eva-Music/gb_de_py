import json
from functools import reduce

companies = {}
final_list = []
average_income = 0

with open("text.txt") as file_read:
    lines = file_read.readlines()
    for line in lines:
        comp_data = line.split(" ")
        companies.update({comp_data[0]: int(comp_data[2])})
    average_income = reduce(lambda x, y: x + y, companies.values()) / len(companies)

final_list.append(companies)
final_list.append(dict({"average_income" : average_income}))

with open("text.json", "w") as file_write:
    json.dump(final_list, file_write)