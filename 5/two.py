file_name = "text.txt"
file_read = open(file_name)
text = file_read.readlines()

print(len(text), " - количество строк")

for t in text:
    word_count = 0
    if t.find(" ") == -1:
        word_count = 1
    else:
        word_count = t.find(" ") + 1
    print("В", text.index(t) + 1, "строке:", "кол-во слов", word_count)