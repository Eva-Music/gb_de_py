file_eng = "text.txt"
file_read = open(file_eng)
text = file_read.readlines()
file_read.close()

with open("new_text.txt", "w") as file_write:
    translate = {"One": "Один", "Two": "Два", "Three": "Три", "Four": "Четыре"}
    for line in text:
        for eng, rus in translate.items():
            if line.find(eng) == 0:
                new_line = line.replace(eng, rus)
                file_write.write(new_line + "\n")
                break
