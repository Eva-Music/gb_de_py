import random
from functools import reduce

with open("text.txt", "w") as file_write:
    random_list = [random.randint(0, 50) for it in range(20)]
    print(random_list)
    file_write.write(' '.join([str(x) for x in random_list]))

with open("text.txt") as file_read:
    numbers = file_read.read().split(" ")
    print(reduce(lambda x, y: int(x) + int(y), numbers))
