from functools import reduce


class Worker:
    def __init__(self, name, surname, position):
        self.name = name
        self.surname = surname
        self.position = position
        self.__income = dict({'wage': 2000, 'bonus': 1000})

    def _get_income(self):
        return self.__income


class Position(Worker):
    def get_full_name(self):
        return self.name + ' ' + self.surname

    def get_total_income(self):
        income_dict = self._get_income()
        return reduce(lambda a, b: a + b, income_dict.values())


position = Position('Name', 'Surname', 'Boss')
print(position.get_full_name())
print(position.get_total_income())
