class Stationery:
    def __init__(self, title):
        self._title = title

    def draw(self):
        print('Запуск отрисовки')


class Pen(Stationery):
    def draw(self):
        print(self._title, 'пишет')


class Pencil(Stationery):
    def draw(self):
        print(self._title, 'пишет')


class Handle(Stationery):
    def draw(self):
        print(self._title, 'пишет')


pen = Pen('pen')
pencil = Pencil('pencil')
handle = Handle('handle')

pen.draw()
pencil.draw()
handle.draw()

