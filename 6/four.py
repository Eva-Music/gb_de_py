class Car:
    def __init__(self, speed, color, name, is_police):
        self._speed = speed
        self._color = color
        self._name = name
        self._is_police = is_police

    def go(self):
        print('Car go')

    def stop(self):
        print('Car stopped')

    def turn(self, direction):
        print('Car turn', direction)

    def show_speed(self):
        return self._speed


class TownCar(Car):
    def show_speed(self):
        if super().show_speed() > 60:
            print('High speed')


class SportCar(Car):
    pass


class WorkCar(Car):
    def show_speed(self):
        if super().show_speed() > 40:
            print('High speed')


class PoliceCar(Car):
    pass


town_car = TownCar(70, 'blue', 'Mercedes', False)
town_car.show_speed()

work_car = WorkCar(30, 'black', 'Kia Rio', False)
work_car.show_speed()

police_car = PoliceCar(100, 'white', 'Toyota', True)
police_car.stop()

sport_car = SportCar(180, 'red', 'Ford', False)
sport_car.go()
