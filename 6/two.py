class Road:
    def __init__(self, length, width):
        self.__length = length
        self.__width = width

    def count_weight(self):
        return self.__length * self.__width * 0.025 * 5


road = Road(20, 5000)
print(road.count_weight())
