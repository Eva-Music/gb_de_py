from itertools import cycle
import time


class TrafficLight:
    def __init__(self):
        self.__color = ['red', 'yellow', 'green']
        pass

    def running(self):
        for item in cycle(self.__color):
            if item == 'red':
                print("Red light")
                time.sleep(7)
            elif item == 'yellow':
                print('Yellow light')
                time.sleep(2)
            else:
                print('Green light')
                time.sleep(10)


my_light = TrafficLight()
my_light.running()