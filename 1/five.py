income = int(input("Выручка: "))
outcome = int(input("Издержки: "))

profit = income - outcome  # Прибыль

if profit > outcome:
    print(f"Рентабельность: {profit / income}")
    emp = int(input("Кол-во сотрудников: "))
    print(f"Прибыль на каждого сотрудника: {profit / emp}")
else:
    print("Предприятие убыточно")
