first_day = int(input("В первый день: "))
record = int(input("Достиг: "))
next_day_result = first_day
days_counter = 1
while next_day_result <= record:
    next_day_result = round(next_day_result, 2)
    proc = (next_day_result * 10) / 100
    next_day_result = round(proc, 2) + next_day_result
    days_counter = days_counter + 1
print(f"На {days_counter} день")

