number = input("Введите число: ")
length = len(number)
max_num = 0
while length > 0:
    part = int(number) % 10
    if part > max_num:
        max_num = part
    number = int(number) // 10
    length = length - 1
print(max_num)
