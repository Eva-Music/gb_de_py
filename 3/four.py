def my_func(x, y):
    return x ** y


def my_func2(x, y):
    while y != 0:
        return my_func2(x * x, y - 1)


print(my_func(4, 5))
print(my_func(2, 4))