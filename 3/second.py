def print_user_info(**kvargs):
    print(', '.join(['{}={!r}'.format(k, v) for k, v in kvargs.items()]))


print_user_info(name="Olya", surname="Mashkova", birth_data="24.08", city="Msk",
                email="olya.maskov@yandex", phone="3506091")
