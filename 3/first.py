def div(a, b):
    if b == 0:
        return "На 0 делить нельзя"
    else:
        return a / b


repeat = "r"
while repeat != "q":
    first_number = int(input("First number: "))
    second_number = int(input("Second_number: "))
    print(div(first_number, second_number))
    repeat = input("Повторить - 'r'\nВыйти - 'q': ")
